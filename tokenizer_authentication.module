<?php

/**
 * Tokenizer authentication module for DRUPAL
 *
 * @author Tomasz Borodziuk <tomasz.borodziuk@amsterdam-standard.pl>
 * @copyright Amsterdam Standard
 * @homepage http://amsterdamstandard.com
 */

// include tokenizer classes
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Exception.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Connector.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Response.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Response/Create.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Response/Verify.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Response/Config.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Storage/SessionStorageInterface.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Storage/GlobalSessionStorage.php';
include __DIR__ . '/vendor/tokenizer-integrations/tokenizer-php/src/Tokenizer/Storage/Exception.php';

// app info
define('APP_ID', variable_get('tokenizer_authentication_app_id', ''));
define('APP_KEY', variable_get('tokenizer_authentication_app_key', ''));

// the path to your tokenize script
define('APP_URL', $GLOBALS['base_url'].'?id=');

if(isset($_GET['id'])) {
    if(isset($_SESSION['ta_user']) && tokenizer_authentication_validate()) tokenizer_authentication_restore_login();
}

/**
 * Implements hook_user_login().
 */
function tokenizer_authentication_user_login(&$edit, $account) {
  
  $tokenizer_roles = variable_get('tokenizer_authentication_roles', array());
  $tokenizer_on = false;
  foreach($account->roles as $key=>$role) {
    if(in_array($key, $tokenizer_roles)) {
        $tokenizer_on = true;
    }
  }
  
  if($tokenizer_on == true) {
    if(!isset($_POST['form_id']) || $_POST['form_id'] != 'user_pass_reset') {           
      tokenizer_authentication_run($account);
    }
  }
}

/**
 * Checks acceptation by tokeneizer
 */
function tokenizer_authentication_validate() {
    try {
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        if(!$id) {
            throw new TokenizerPhp\Tokenizer\Exception("No url id found");
        }

        $tokenizer = new TokenizerPhp\Tokenizer([
            'app_id'  => APP_ID,
            'app_key' => APP_KEY,
        ]);

        if($tokenizer->getSessionStorage()->get('tokenizer_id') === null) {
            watchdog('tokenizer_authentication', 'No tokenizer session found');
            throw new TokenizerPhp\Tokenizer\Exception("No tokenizer session found");
        }

        if($tokenizer->getSessionStorage()->get('tokenizer_id') != $id) {
            watchdog('tokenizer_authentication', 'Tokenizer session does not match url id');
            throw new TokenizerPhp\Tokenizer\Exception("Tokenizer session does not match url id");
        }

        if( $tokenizer->verifyAuth($id) ) {
            
            $result = true;
        } else {
            drupal_set_message(t('Login rejected'), 'error');
            $result = false;
        }

    } catch(TokenizerPhp\Tokenizer\Exception $e) {
        watchdog('tokenizer_authentication', 'Tokeneizer exception: '.$e->getMessage());
        $result = false;
    }
    
    return $result;
}

/**
 * Initializes tokenizer authentication
 */
function tokenizer_authentication_run($account) {
    if(APP_ID!='') {
      tokenizer_authentication_switch_to_anonymous();

      try {
          $tokenizer = new TokenizerPhp\Tokenizer([
              'app_id'  => APP_ID,
              'app_key' => APP_KEY,
          ]);

          $tokenizer->createAuth($account->mail, APP_URL, $redirect = true);      
      } catch(TokenizerPhp\Tokenizer\Exception $e) {
          $e_message = $e->getMessage();
          watchdog('tokenizer_authentication', 'User login exception: '.$e_message);
          if($e_message == 'Tokenizer error: User was not authenticated') drupal_set_message(t('You have been logged out because Tokenizer has not recognized your e-mail. Please set your e-mail in Tokenizer before login!'), 'error');
          else drupal_set_message(t('Authentication failed. You have been logged out.'), 'error');
      }
    }
}

/**
 * Pauses user login
 */
function tokenizer_authentication_switch_to_anonymous() {
    global $user;
    $ta_user = $user;
    watchdog('tokenizer_authentication', 'Session closed for %name.', array('%name' => $user->name));
    module_invoke_all('user_logout', $user);
    session_destroy();
    $user = drupal_anonymous_user();
    session_start();
    $_SESSION['ta_user'] = $ta_user;
}

/**
 * Restores user login
 */
function tokenizer_authentication_restore_login() {
    if(!isset($_SESSION['ta_user'])) return false;  

    global $user;
    
    $user = $_SESSION['ta_user'];
    unset($_SESSION['ta_user']);

    $user->login = REQUEST_TIME;
    db_update('users')
        ->fields(array('login' => $user->login))
        ->condition('uid', $user->uid)
        ->execute();

    drupal_session_regenerate();
    watchdog('tokenizer_authentication', 'Session set for %name.', array('%name' => $user->name));
}

/**
 * Implements hook_menu().
 */
function tokenizer_authentication_menu() {
  $items = array();

  $items['admin/config/people/tokenizer_authentication'] = array(
    'title' => 'Tokenizer authentication',
    'description' => 'Configuration for tokenizer authentication module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tokenizer_authentication_configuration_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function tokenizer_authentication_configuration_form() {
  $form = array();

  $form['tokenizer_authentication_app_key'] = array(
    '#type' => 'textfield',
    '#title' => t('key'),
    '#default_value' => variable_get('tokenizer_authentication_app_key', ''),
    '#size' => 32,
    '#maxlength' => 32,
    '#description' => t("Tokenizer service key"),
    '#required' => TRUE,
  );
  
  $form['tokenizer_authentication_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('id'),
    '#default_value' => variable_get('tokenizer_authentication_app_id', ''),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t("Tokenizer service id"),
    '#required' => TRUE,
  );

  $form['tokenizer_authentication_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autenticate all users with e-mail and Tokenizer only'),
    '#default_value' => variable_get('tokenizer_authentication_mode', 0),
    '#description' => t("Disables username/password authentication")
  );
  
  if(!variable_get('tokenizer_authentication_mode',0)) {
    $user_roles = user_roles();
    foreach($user_roles as $id => $name) if($name=='anonymous user') unset($user_roles[$id]);

    $form['tokenizer_authentication_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Enable for users with selected roles'),
      '#description' => t('Select user roles for which a Tokenizer authentication should be enabled'),
      '#options' => $user_roles,
      '#default_value' => variable_get('tokenizer_authentication_roles', array())
    );
  }

  return system_settings_form($form);
}

function tokenizer_authentication_configuration_form_validate($form, &$form_state) {
    global $user;
    if(variable_get('tokenizer_authentication_app_id','')!= $form_state['values']['tokenizer_authentication_app_id']
    || variable_get('tokenizer_authentication_app_key','')!= $form_state['values']['tokenizer_authentication_app_key']) {
        try {
            $tokenizer = new TokenizerPhp\Tokenizer([
                'app_id'  => $form_state['values']['tokenizer_authentication_app_id'],
                'app_key' => $form_state['values']['tokenizer_authentication_app_key'],
            ]);

            $tokenizer->createAuth($user->mail, APP_URL, $redirect = false);      
            drupal_set_message(t('New credentials are verified. Now in your Tokenizer app should appear acceptation request. In this case you can ingore it.'));
        } catch(TokenizerPhp\Tokenizer\Exception $e) {
            $message = trim($e->getMessage());
            watchdog('tokenizer_authentication', 'Configuration exception: '.$message);
            form_set_error('tokenizer_authentication_email', t('Connection with Tokenizer failed so these credentials cannot be saved! Please check the email "New Tokenizer service created" received after adding your service to Tokenizer.')."<br/>".$message);
            if($message == "Tokenizer error: User was not authenticated") {
                drupal_set_message(t('Please set your email "user_mail" in Tokenizer before changing this configuration.', array('user_mail' => $user->mail)),'error');
            }
            return false;
        }
    }
    if(variable_get('tokenizer_authentication_mode',0)!= $form_state['values']['tokenizer_authentication_mode']) {
        drupal_flush_all_caches();
        drupal_set_message(t('Drupal cache flushed.'));
        watchdog('tokenizer_authentication', 'Authentication mode changed.');
    }
}

function tokenizer_authentication_email_validate($email) {
    try {
        $tokenizer = new TokenizerPhp\Tokenizer([
            'app_id'  => APP_ID,
            'app_key' => APP_KEY,
        ]);

        $tokenizer->createAuth($email, APP_URL, $redirect = false);      
        return true;
    } catch(TokenizerPhp\Tokenizer\Exception $e) {
        $message = trim($e->getMessage());
        watchdog('tokenizer_authentication', 'Email validation: '.$message);
        if($message == "Tokenizer error: User was not authenticated") {
            drupal_set_message(t('We could not authenticate your email "user_mail". Please set it in Tokenizer and try again.', array('user_mail' => $email)),'error');
        }
        return false;
    }
}

/**
 * Login with E-mail and Tokenizer only
 */
if(variable_get('tokenizer_authentication_mode',0)) {
    /**
     * Implements hook_form_alter().
     */
    function tokenizer_authentication_form_alter(&$form, &$form_state, $form_id) {
      switch ($form_id) {
        case 'user_login' :
        case 'user_login_block' :
          $form = drupal_get_form('tokenizer_authentication_login');
        break;

        case 'user_register_form' :
          $form['account']['pass']['#type'] = 'hidden';
          $form['account']['pass']['#default_value'] = tokenizer_authentication_generate_password();
          array_unshift($form['#validate'], 'tokenizer_authentication_register_validate');
        break;

        case 'user_profile_form' :
          $form['account']['mail']['#description'] = t('Your Tokeneizer-authenticated E-mail.');
          $validate_unset = array_search('user_validate_current_pass', $form['#validate']);
          unset($form['#validate'][$validate_unset], $form['account']['pass'], $form['account']['current_pass']);
          array_unshift($form['#validate'], 'tokenizer_authentication_profile_validate');
        break;
      }
    }

    function tokenizer_authentication_login() {
      global $user;

      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('E-mail address'),
        '#size' => 20,
        '#maxlength' => max(USERNAME_MAX_LENGTH, EMAIL_MAX_LENGTH),
        '#required' => TRUE,
      );
      
      if (variable_get('user_register', 1)) {
        $form['name']['#description'] = t('Log in or ').'<a href="'.$GLOBALS['base_url'].'/?q=user/register">'.t('register').'</a>';
      }

      $form['actions'] = array('#type' => 'actions');
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Log in'),
      );

      if (variable_get('tokenizer_authentication_show_help', 1) && variable_get('maintenance_mode', 0) == 0) {
        $form['tokenizer_authentication_help_link'] = array(
          '#type' => 'markup',
          '#markup' => theme('tokenizer_authentication_help_link'),
          '#weight' => 1000,
        );
      }

      return $form;
    }

    function tokenizer_authentication_login_validate($form, &$form_state) {
      $name = trim($form_state['values']['name']);
      $user_array = user_load_multiple(array(), array('mail' => $name, 'status' => '1'));
      if(count($user_array)==1) foreach($user_array as $user) {
        $account = user_load($user->uid);
        $_SESSION['ta_user'] = $account;
        tokenizer_authentication_restore_login();
        tokenizer_authentication_run($account);
      }
    }

    function tokenizer_authentication_register_validate($form, &$form_state) {
        if(!tokenizer_authentication_email_validate($form_state['values']['mail'])) form_set_error('Not authenticated e-mail!');
        form_set_value(array('#parents' => array('pass')), tokenizer_authentication_generate_password(), $form_state);
    }

    function tokenizer_authentication_profile_validate($form, &$form_state) {
        if(!tokenizer_authentication_email_validate($form_state['values']['mail'])) form_set_error('Not authenticated e-mail!');
    }

    function tokenizer_authentication_generate_password() {
        return md5(user_password().'tokenizer_authentication'.rand());
    }
}
