<?php

/**
 * Tokenizer Response Class
 * 
 * Parses the response of a Tokenizer api call into the corresponding object
 *
 * @author Frank Broersen <frank@amsterdamstandard.com>
 * @copyright Amsterdam Standard
 * @homepage http://amsterdamstandard.com
 */

namespace TokenizerPhp\Tokenizer;

use TokenizerPhp\Tokenizer\Exception;
use TokenizerPhp\Tokenizer\Response\Create;
use TokenizerPhp\Tokenizer\Response\Verify;
use TokenizerPhp\Tokenizer\Response\Config;

class Response
{
    /**
     * Method for parsing type and received json data from Tokenizer API
     * @param $type
     * @param $json
     * @return Config|Create|Verify
     * @throws Exception
     */
    public static function parse($type, $json)
    {
        $data = json_decode($json, 1);    
        if(isset($data['status_code'])) {
            throw new Exception("Tokenizer error: " . $data['error_messages']);
        }
        
        if($type == 'create') {
            return new Create($data);
        } 
        if($type == 'verify') {
            return new Verify($data);
        }
        if($type == 'config') {
            return new Config($data);
        }
        
        throw new Exception("Invalid tokenizer response type ($type)");

    }
    
}