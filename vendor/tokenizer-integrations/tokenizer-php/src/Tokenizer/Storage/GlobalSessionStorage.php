<?php
/**
 * GlobalStorage Class
 *
 * @author Mateusz Aniolek <mateusz.aniolek@amsterdam-standard.pl>
 * @copyright Amsterdam Standard
 */

namespace TokenizerPhp\Tokenizer\Storage;


class GlobalSessionStorage implements SessionStorageInterface
{
    public function get($key)
    {
        return (isset($_SESSION[$key]) ? $_SESSION[$key] : null);
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

}